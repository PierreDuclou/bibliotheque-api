FROM maven:3.6.2-jdk-11-slim
WORKDIR /app
COPY . .
RUN mvn dependency:go-offline \
    && mvn package -DskipTests
EXPOSE 8090
ENTRYPOINT ["java", "-jar", "target/bibliotheque-api-0.0.1-SNAPSHOT.jar"]
