package org.miage.bibliotheque.boundary;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.entity.Exemplaire;
import org.miage.bibliotheque.exception.ExemplaireValidationException;
import org.miage.bibliotheque.exception.OeuvreValidationException;
import org.miage.bibliotheque.service.ExemplaireService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/exemplaires", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class ExemplaireBoundary {

    private final ExemplaireService exemplaireService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(exemplaireService.findAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") String id) {
        return exemplaireService.findById(id)
                .map(exemplaire -> new ResponseEntity<>(exemplaire, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<?> store(@RequestBody Map<String, String> data) {
        try {
            exemplaireService.saveMany(data.get("idOeuvre"), data.get("quantite"));
        } catch (OeuvreValidationException | ExemplaireValidationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<?> update(@RequestBody Exemplaire exemplaire, @PathVariable String id) {
        if (exemplaireService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        try {
            exemplaireService.update(exemplaire, id);
        } catch (OeuvreValidationException | ExemplaireValidationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<?> destroy(@PathVariable String id) {
        exemplaireService.deleteById(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
