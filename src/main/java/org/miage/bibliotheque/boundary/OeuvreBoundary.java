package org.miage.bibliotheque.boundary;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.entity.Oeuvre;
import org.miage.bibliotheque.exception.AuteurValidationException;
import org.miage.bibliotheque.exception.OeuvreValidationException;
import org.miage.bibliotheque.service.OeuvreService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Access;
import javax.persistence.AccessType;

@RestController
@RequestMapping(value = "/oeuvres", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Access(AccessType.FIELD)
public class OeuvreBoundary {

    private final OeuvreService oeuvreService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(oeuvreService.findAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") String id) {
        return oeuvreService.findById(id)
                .map(oeuvre -> new ResponseEntity<>(oeuvre, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<?> store(@RequestBody Oeuvre oeuvre) {
        try {
            oeuvreService.save(oeuvre);
        } catch (AuteurValidationException | OeuvreValidationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<?> update(@PathVariable String id, @RequestBody Oeuvre oeuvre) {
        if (oeuvreService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        try {
            oeuvreService.update(oeuvre, id);
        } catch (AuteurValidationException | OeuvreValidationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<?> destroy(@PathVariable String id) {
        oeuvreService.deleteById(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
