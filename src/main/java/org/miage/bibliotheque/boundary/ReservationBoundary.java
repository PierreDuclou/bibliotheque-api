package org.miage.bibliotheque.boundary;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.exception.ReservationValidationException;
import org.miage.bibliotheque.service.ReservationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/reservations", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class ReservationBoundary {

    private final ReservationService reservationService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(reservationService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> store(@RequestBody Map<String, String> data) {
        try {
            reservationService.save(data.get("idOeuvre"), data.get("idUsager"));
        } catch (ReservationValidationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("{id}")
    public void destroy(@PathVariable String id) {
        reservationService.deleteById(id);
    }

}
