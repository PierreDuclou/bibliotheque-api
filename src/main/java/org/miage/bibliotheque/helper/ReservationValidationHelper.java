package org.miage.bibliotheque.helper;

import org.miage.bibliotheque.entity.Reservation;
import org.miage.bibliotheque.exception.ReservationValidationException;
import org.miage.bibliotheque.service.OeuvreService;
import org.miage.bibliotheque.service.ReservationService;
import org.miage.bibliotheque.service.UsagerService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ReservationValidationHelper implements ApplicationContextAware, InitializingBean {

    private ApplicationContext context;

    @Autowired
    private OeuvreValidationHelper oeuvreValidationHelper;

    @Autowired
    private UsagerValidationHelper usagerValidationHelper;

    @Autowired
    private OeuvreService oeuvreService;

    @Autowired
    private UsagerService usagerService;

    private ReservationService reservationService;

    @Override
    public void afterPropertiesSet() throws Exception {
        reservationService = context.getBean(ReservationService.class);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    public void validate(String idOeuvre, String idUsager) {
        if (idOeuvre == null || idOeuvre.isBlank()) {
            throw new ReservationValidationException("L'identifiant de l'oeuvre ne peut pas être vide.");
        }

        var oeuvreMaybe = oeuvreService.findById(idOeuvre);

        if (oeuvreMaybe.isEmpty()) {
            throw new ReservationValidationException("Oeuvre inconnue : " + idOeuvre);
        }

        if (idUsager == null || idUsager.isBlank()) {
            throw new ReservationValidationException("L'identifiant de l'usager ne peut pas être vide.");
        }

        var usagerMaybe = usagerService.findById(idUsager);

        if (usagerMaybe.isEmpty()) {
            throw new ReservationValidationException("Usager inconnu : " + idUsager);
        }

        for (Reservation existing : reservationService.findAll()) {
            if (existing.getOeuvre().equals(oeuvreMaybe.get()) && existing.getUsager().equals(usagerMaybe.get())) {
                throw new ReservationValidationException("L'usager ne peut réserver l'oeuvre qu'une seule fois.");
            }
        }
    }

}
