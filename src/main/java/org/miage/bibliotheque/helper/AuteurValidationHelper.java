package org.miage.bibliotheque.helper;

import org.miage.bibliotheque.entity.Auteur;
import org.miage.bibliotheque.exception.AuteurValidationException;
import org.springframework.stereotype.Component;

@Component
public class AuteurValidationHelper {

    public void validate(Auteur auteur) {
        if (auteur == null) {
            throw new AuteurValidationException("L'auteur ne peut pas être nul.");
        }

        if (auteur.getId() == null || auteur.getId().isBlank()) {
            throw new AuteurValidationException("L'identifiant ne peut pas être vide.");
        }

        if (auteur.getNom() == null || auteur.getNom().isBlank()) {
            throw new AuteurValidationException("Le nom ne peut pas être vide.");
        }

        if (auteur.getPrenom() == null || auteur.getPrenom().isBlank()) {
            throw new AuteurValidationException("Le prénom ne peut pas être vide.");
        }
    }

}
