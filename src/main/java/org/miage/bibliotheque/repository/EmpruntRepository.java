package org.miage.bibliotheque.repository;

import org.miage.bibliotheque.entity.Emprunt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmpruntRepository extends JpaRepository<Emprunt, String> {
}
