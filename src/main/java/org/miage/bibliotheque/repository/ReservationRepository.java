package org.miage.bibliotheque.repository;

import org.miage.bibliotheque.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReservationRepository extends JpaRepository<Reservation, String> {
}
