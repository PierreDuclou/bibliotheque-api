package org.miage.bibliotheque.repository;

import org.miage.bibliotheque.entity.Exemplaire;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExemplaireRepository extends JpaRepository<Exemplaire, String> {
}
