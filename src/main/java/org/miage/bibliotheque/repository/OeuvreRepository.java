package org.miage.bibliotheque.repository;

import org.miage.bibliotheque.entity.Oeuvre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OeuvreRepository extends JpaRepository<Oeuvre, String> {
}
