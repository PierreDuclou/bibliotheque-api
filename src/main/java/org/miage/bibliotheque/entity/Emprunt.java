package org.miage.bibliotheque.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "emprunts")
public class Emprunt implements Serializable {

    @Id
    String id;

    LocalDate dateDebut;

    LocalDate dateFin;

    @ManyToOne
    @JoinColumn(name = "usager_id")
    @JsonIgnoreProperties("emprunts")
    Usager usager;

    @ManyToOne
    @JoinColumn(name = "exemplaire_id")
    @JsonIgnoreProperties("emprunts")
    Exemplaire exemplaire;

    public Long getRetard() {
        return ChronoUnit.DAYS.between(dateFin, LocalDate.now());
    }

}
