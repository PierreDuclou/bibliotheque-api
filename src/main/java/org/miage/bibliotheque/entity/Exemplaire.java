package org.miage.bibliotheque.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "exemplaires")
public class Exemplaire implements Serializable {

    @Id
    String id;

    @Column(nullable = false)
    EtatExemplaire etat;

    @OneToMany(mappedBy = "exemplaire", cascade = CascadeType.REMOVE)
    @JsonIgnoreProperties("exemplaire")
    List<Emprunt> emprunts;

    @ManyToOne
    @JoinColumn(name = "oeuvre_id")
    @JsonIgnoreProperties("exemplaires")
    Oeuvre oeuvre;

}
