package org.miage.bibliotheque.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_oeuvre")
@Table(name = "oeuvres")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Livre.class, name = "Livre"),
        @JsonSubTypes.Type(value = Magazine.class, name = "Magazine")
})
public class Oeuvre implements Serializable {

    @Id
    String id;

    String titre;

    @OneToMany(mappedBy = "oeuvre", cascade = CascadeType.REMOVE)
    @JsonIgnoreProperties("oeuvre")
    List<Exemplaire> exemplaires;

    @OneToMany(mappedBy = "oeuvre", cascade = CascadeType.REMOVE)
    @JsonIgnoreProperties(value = {"oeuvre"})
    List<Reservation> reservations;

}
