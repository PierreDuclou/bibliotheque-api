package org.miage.bibliotheque.entity;

public enum EtatExemplaire {

    BON("Bon"),
    MAUVAIS("Mauvais");

    private final String etat;

    EtatExemplaire(String etat) {
        this.etat = etat;
    }

    @Override
    public String toString() {
        return etat;
    }

}
