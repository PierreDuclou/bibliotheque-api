package org.miage.bibliotheque.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@DiscriminatorValue("Livre")
@JsonTypeName(value = "Livre")
public class Livre extends Oeuvre {

    String editeur;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "oeuvres_auteurs",
            joinColumns = @JoinColumn(name = "oeuvre_id"),
            inverseJoinColumns = @JoinColumn(name = "auteur_id")
    )
    @JsonIgnoreProperties("livres")
    List<Auteur> auteurs;

}