package org.miage.bibliotheque.exception;

public class UsagerValidationException extends RuntimeException {

    public UsagerValidationException(String message) {
        super(message);
    }

}
