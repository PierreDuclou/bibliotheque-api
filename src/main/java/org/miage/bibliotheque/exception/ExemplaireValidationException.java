package org.miage.bibliotheque.exception;

public class ExemplaireValidationException extends RuntimeException {

    public ExemplaireValidationException(String message) {
        super(message);
    }

}
