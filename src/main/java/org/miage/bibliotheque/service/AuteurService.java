package org.miage.bibliotheque.service;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.entity.Auteur;
import org.miage.bibliotheque.helper.AuteurValidationHelper;
import org.miage.bibliotheque.repository.AuteurRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AuteurService {

    private final AuteurRepository auteurRepository;

    private final AuteurValidationHelper auteurValidationHelper;

    public List<Auteur> findAll() {
        return auteurRepository.findAll();
    }

    public Optional<Auteur> findById(String id) {
        return auteurRepository.findById(id);
    }

    public Auteur save(Auteur auteur) {
        auteur.setId(UUID.randomUUID().toString());
        auteurValidationHelper.validate(auteur);

        return auteurRepository.save(auteur);
    }

    public Auteur update(Auteur auteur, String id) {
        auteur.setId(id);
        auteurValidationHelper.validate(auteur);

        return auteurRepository.save(auteur);
    }

    public void deleteAll() {
        auteurRepository.deleteAll();
    }

    public void delete(String id) {
        auteurRepository.findById(id).ifPresent(auteurRepository::delete);
    }

}
