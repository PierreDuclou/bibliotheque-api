package org.miage.bibliotheque.service;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.entity.EtatExemplaire;
import org.miage.bibliotheque.entity.Exemplaire;
import org.miage.bibliotheque.helper.ExemplaireValidationHelper;
import org.miage.bibliotheque.repository.ExemplaireRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ExemplaireService {

    private final ExemplaireRepository exemplaireRepository;

    private final ExemplaireValidationHelper exemplaireValidationHelper;

    private final OeuvreService oeuvreService;

    public List<Exemplaire> findAll() {
        return exemplaireRepository.findAll();
    }

    public Optional<Exemplaire> findById(String id) {
        return exemplaireRepository.findById(id);
    }

    public void saveMany(String idOeuvre, String quantite) {
        exemplaireValidationHelper.validate(idOeuvre, quantite);

        var oeuvre = oeuvreService.findById(idOeuvre).get();
        Exemplaire exemplaire;

        for (int i = 1; i <= Integer.parseInt(quantite); i++) {
            exemplaire = new Exemplaire(UUID.randomUUID().toString(), EtatExemplaire.BON, null, oeuvre);
            exemplaireRepository.save(exemplaire);
        }
    }

    public Exemplaire update(Exemplaire exemplaire, String id) {
        exemplaire.setId(id);
        exemplaireValidationHelper.validate(exemplaire);

        return exemplaireRepository.save(exemplaire);
    }

    public void deleteById(String id) {
        exemplaireRepository.findById(id).ifPresent(exemplaireRepository::delete);
    }

    public void deleteAll() {
        exemplaireRepository.deleteAll();
    }

    public Exemplaire save(Exemplaire exemplaire) {
        exemplaire.setId(UUID.randomUUID().toString());
        exemplaire.setEtat(EtatExemplaire.BON);
        exemplaireValidationHelper.validate(exemplaire);

        return exemplaireRepository.save(exemplaire);
    }
}
