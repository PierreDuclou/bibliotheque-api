package org.miage.bibliotheque.service;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.entity.Oeuvre;
import org.miage.bibliotheque.helper.OeuvreValidationHelper;
import org.miage.bibliotheque.repository.OeuvreRepository;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OeuvreService {

    private final OeuvreRepository oeuvreRepository;

    private final OeuvreValidationHelper oeuvreValidationHelper;

    public List<Oeuvre> findAll() {
        return oeuvreRepository.findAll();
    }

    public Optional<Oeuvre> findById(String id) {
        return oeuvreRepository.findById(id);
    }

    public Optional<Oeuvre> findOne(Example<Oeuvre> example) {
        return oeuvreRepository.findOne(example);
    }

    public Oeuvre save(Oeuvre oeuvre) {
        oeuvre.setId(UUID.randomUUID().toString());
        oeuvreValidationHelper.validate(oeuvre);

        return oeuvreRepository.save(oeuvre);
    }

    public Oeuvre update(Oeuvre oeuvre, String id) {
        oeuvre.setId(id);
        oeuvreValidationHelper.validate(oeuvre);

        return oeuvreRepository.save(oeuvre);
    }

    public void deleteAll() {
        oeuvreRepository.deleteAll();
    }

    public void deleteById(String id) {
        oeuvreRepository.findById(id).ifPresent(oeuvreRepository::delete);
    }

}
