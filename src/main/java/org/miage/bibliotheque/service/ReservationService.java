package org.miage.bibliotheque.service;

import lombok.RequiredArgsConstructor;
import org.miage.bibliotheque.entity.Reservation;
import org.miage.bibliotheque.helper.ReservationValidationHelper;
import org.miage.bibliotheque.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ReservationService {

    private final ReservationRepository reservationRepository;

    private ReservationValidationHelper reservationValidationHelper;

    private final OeuvreService oeuvreService;

    private final UsagerService usagerService;

    @Autowired
    public void setReservationValidationHelper(ReservationValidationHelper reservationValidationHelper) {
        this.reservationValidationHelper = reservationValidationHelper;
    }

    public List<Reservation> findAll() {
        return reservationRepository.findAll();
    }

    public Optional<Reservation> findById(String id) {
        return reservationRepository.findById(id);
    }

    public Optional<Reservation> findByExample(Example<Reservation> example) {
        return reservationRepository.findOne(example);
    }

    public Reservation save(String idOeuvre, String idUsager) {
        reservationValidationHelper.validate(idOeuvre, idUsager);

        var oeuvre = oeuvreService.findById(idOeuvre).get();
        var usager = usagerService.findById(idUsager).get();

        var reservation = new Reservation(UUID.randomUUID().toString(), LocalDate.now(), usager, oeuvre);

        return reservationRepository.save(reservation);
    }

    public void deleteById(String id) {
        reservationRepository.findById(id).ifPresent(reservationRepository::delete);
    }

    public void deleteAll() {
        reservationRepository.deleteAll();
    }

    public void delete(Reservation reservation) {
        reservationRepository.delete(reservation);
    }
}
