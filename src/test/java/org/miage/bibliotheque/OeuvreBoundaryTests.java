package org.miage.bibliotheque;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miage.bibliotheque.entity.Auteur;
import org.miage.bibliotheque.entity.Livre;
import org.miage.bibliotheque.entity.Magazine;
import org.miage.bibliotheque.service.AuteurService;
import org.miage.bibliotheque.service.OeuvreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OeuvreBoundaryTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private OeuvreService oeuvreService;

    @Autowired
    private AuteurService auteurService;

    @Before
    public void reset() {
        oeuvreService.deleteAll();
        auteurService.deleteAll();
    }

    @Test
    public void getOne_ShouldProvideAuteurs_IfOeuvreIsALivre() {
        var a1 = auteurService.save(new Auteur(null, "Zola", "Emile", null));
        var a2 = auteurService.save(new Auteur(null, "Remi", "Georges", null));
        var livre = new Livre("Some editor", Arrays.asList(a1, a2));
        livre.setTitre("Tintin au Tibet");

        oeuvreService.save(livre);

        var responseEntity = restTemplate.getForEntity("/oeuvres/" + livre.getId(), Livre.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(responseEntity.getBody()).getAuteurs()).contains(a1);
        assertThat(responseEntity.getBody().getAuteurs()).contains(a2);
    }

    @Test
    public void store_ShouldCastOeuvreType_IfOeuvreIsValid() {
        var auteur = auteurService.save(new Auteur("someid", "Zola", "Emile", null));

        var livre = new Livre("editeur", Collections.singletonList(auteur));
        livre.setTitre("foo");

        int numero = 12;
        var magazine = new Magazine(numero);
        magazine.setTitre("bar");

        var livreResponse = restTemplate.postForEntity("/oeuvres", livre, String.class);
        var magazineResponse = restTemplate.postForEntity("/oeuvres", magazine, String.class);

        assertThat(livreResponse.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(oeuvreService.findAll()).isNotEmpty();
        livre = (Livre) oeuvreService.findAll().get(0);
        assertThat(livre.getAuteurs()).isNotEmpty();

        assertThat(magazineResponse.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        magazine = (Magazine) oeuvreService.findAll().get(1);
        assertThat(magazine.getNumero()).isEqualTo(numero);
    }

    @Test
    public void storeLivre_ShouldFail_IfAuteurDoesntExist() {
        var livre = new Livre("editeur", Collections.singletonList(new Auteur("doesntexist", "nom", "prenom", null)));
        livre.setTitre("Tintin chez les romains");

        var responseEntity = restTemplate.postForEntity("/oeuvres", livre, String.class);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void update_ShouldPreserveSubclassAttributes_IfOeuvreIsValid() {
        int numero = 12;
        var magazine = new Magazine(numero);
        magazine.setTitre("foo");
        magazine = (Magazine) oeuvreService.save(magazine);
        magazine.setTitre("bar");

        restTemplate.put("/oeuvres/" + magazine.getId(), magazine);

        assertThat(oeuvreService.findById(magazine.getId())).isNotEmpty();
        magazine = (Magazine) oeuvreService.findAll().get(0);
        assertThat(magazine.getNumero()).isEqualTo(numero);
    }

}
