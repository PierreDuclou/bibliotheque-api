package org.miage.bibliotheque;

import lombok.RequiredArgsConstructor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miage.bibliotheque.entity.Exemplaire;
import org.miage.bibliotheque.entity.Magazine;
import org.miage.bibliotheque.entity.Usager;
import org.miage.bibliotheque.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RequiredArgsConstructor
public class EmpruntBoundaryTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EmpruntService empruntService;

    @Autowired
    private UsagerService usagerService;

    @Autowired
    private OeuvreService oeuvreService;

    @Autowired
    private ExemplaireService exemplaireService;

    @Autowired
    private ReservationService reservationService;

    @Before
    public void reset() {
        empruntService.deleteAll();
        usagerService.deleteAll();
        exemplaireService.deleteAll();
        oeuvreService.deleteAll();
    }

    @Test
    public void getAll_ShouldAllEmprunts_IfAny() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);
        exemplaireService.save(new Exemplaire(null, null, null, oeuvre));
        var emprunt = empruntService.save(oeuvre.getId(), usager.getId(), "2019-11-16", "2019-12-04");

        var responseEntity = restTemplate.getForEntity("/emprunts", String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).contains(emprunt.getId());
    }

    @Test
    public void store_ShouldSucceed_IfDataValid() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);
        exemplaireService.save(new Exemplaire(null, null, null, oeuvre));

        var data = Map.of(
                "idUsager", usager.getId(),
                "idOeuvre", oeuvre.getId(),
                "dateDebut", "2019-11-16",
                "dateFin", "2019-12-04"
        );

        var responseEntity = restTemplate.postForEntity("/emprunts", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(empruntService.findAll()).isNotEmpty();
    }

    @Test
    public void store_ShouldDeleteCorrespondingReservation_IfAny() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);
        exemplaireService.save(new Exemplaire(null, null, null, oeuvre));
        var reservation = reservationService.save(oeuvre.getId(), usager.getId());

        var data = Map.of(
                "idUsager", usager.getId(),
                "idOeuvre", oeuvre.getId(),
                "dateDebut", "2019-11-16",
                "dateFin", "2019-12-04"
        );

        var responseEntity = restTemplate.postForEntity("/emprunts", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(reservationService.findById(reservation.getId())).isEmpty();
    }

    @Test
    public void store_ShouldFail_IfUsagerNotValid() {
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);
        exemplaireService.save(new Exemplaire(null, null, null, oeuvre));

        var data = Map.of(
                "idUsager", "doesntexist",
                "idOeuvre", oeuvre.getId(),
                "dateDebut", "2019-11-16",
                "dateFin", "2019-12-04"
        );

        var responseEntity = restTemplate.postForEntity("/emprunts", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void store_ShouldFail_IfOeuvreNotValid() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);
        exemplaireService.save(new Exemplaire(null, null, null, oeuvre));

        var data = Map.of(
                "idUsager", usager.getId(),
                "idOeuvre", "doesntexist",
                "dateDebut", "2019-11-16",
                "dateFin", "2019-12-04"
        );

        var responseEntity = restTemplate.postForEntity("/emprunts", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void store_ShouldFail_IfDateCannotBeParsed() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);
        exemplaireService.save(new Exemplaire(null, null, null, oeuvre));

        var data = Map.of(
                "idUsager", usager.getId(),
                "idOeuvre", oeuvre.getId(),
                "dateDebut", "cannot be parsed",
                "dateFin", "2019-12-04"
        );

        var responseEntity = restTemplate.postForEntity("/emprunts", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void store_ShouldFail_IfPeriodNotValid() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);
        exemplaireService.save(new Exemplaire(null, null, null, oeuvre));

        var data = Map.of(
                "idUsager", usager.getId(),
                "idOeuvre", oeuvre.getId(),
                "dateDebut", "2019-11-16",
                "dateFin", "2019-10-04"
        );

        var responseEntity = restTemplate.postForEntity("/emprunts", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void store_ShouldFail_IfPeriodOverlappingExistingOne() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);
        exemplaireService.save(new Exemplaire(null, null, null, oeuvre));

        var start = "2019-11-16";
        var end = "2019-12-04";
        empruntService.save(oeuvre.getId(), usager.getId(), start, end);

        var data = Map.of(
                "idUsager", usager.getId(),
                "idOeuvre", oeuvre.getId(),
                "dateDebut", start,
                "dateFin", end
        );

        var responseEntity = restTemplate.postForEntity("/emprunts", data, String.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void destroy_ShouldSucceed_IfEmpruntExist() {
        var usager = usagerService.save(new Usager("foobar@email.fr", "bar", "foo", "0612345678"));
        var magazine = new Magazine(13);
        magazine.setTitre("TéléLoisir");
        var oeuvre = oeuvreService.save(magazine);
        exemplaireService.save(new Exemplaire(null, null, null, oeuvre));
        var emprunt = empruntService.save(oeuvre.getId(), usager.getId(), "2019-11-16", "2019-12-04");

        restTemplate.delete("/emprunts/" + emprunt.getId(), String.class);

        assertThat(empruntService.findById(emprunt.getId()).isEmpty()).isTrue();
    }

}
