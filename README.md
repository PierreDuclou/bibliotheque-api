# bibliotheque-api
## Description
Projet de M2 MIAGE (SID) - Applications Distribuées

## Démarrage du service
Pour fonctionner, l'API doit pouvoir se connecter à une base de données en utilisant les variables d'environnement suivantes :
```
DB_DRIVER   → Driver JDBC (ex: "postgres")
DB_HOST     → Adresse du serveur (ex: "localhost")
DB_PORT     → Port du serveur (ex: "5432")
DB_NAME     → Nom de la base de données (ex: "bibal")
DB_USER     → Nom d'utilisateur (ex: "postgres")
DB_PASSWORD → Mot de passe (ex: "postgres")
```

Au lancement, l'application créera les tables automatiquement si elles n'existent pas déjà.

Trois sont options possibles pour lancer l'application :
- Utiliser Docker et Docker Compose
- Sur les machines UNIX, utiliser le script appelé `run` à la racine du projet
- Utiliser Maven et PostgreSQL → créer la base de données et exporter les variables d'environnement manuellement puis lancer l'application avec Maven

### Docker & Docker Compose
Une configuration pour Docker Compose est disponible à la racine du projet. Cette dernière permet de simuler un environnment de production en encapsulant l'API et la base de données dans des conteneurs.
Pour fonctionner, elle nécessite la création d'un fichier nommé `.env` à la racine du projet contenant les variables d'environnement. Exemple :

```shell
# Variables pour l'API Java
DB_DRIVER=postgresql
DB_HOST=db # "db" est le nom de l'hôte virtuel défini dans le fichier docker-compose.yml
DB_PORT=5432
DB_NAME=bibliotheque
DB_USER=postgres
DB_PASSWORD=postgres

# Variables pour le serveur Postgres
POSTGRES_PORT=5432
POSTGRES_DB=bibliotheque
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
```

Une fois le fichier crée, lancer la commande suivante à la racine du projet :

```shell
docker-compose up
```
Cette commande crée un conteneur pour la base de données et un autre pour l'application Spring. Un réseau virtuel permettant l'inter-communication des conteneurs est également crée.
**Les dépendances Maven seront téléchargées à chaque fois que le conteneur sera (re)construit**.

### Maven, Shell & Docker
Pour lancer l'application dans un environnement de développement, exécuter le script `run` situé à la racine du projet :

```shell
./run
```

Ce-dernier exporte les variables d'environnements nécessaires, crée le conteneur de base de données puis lance l'application avec Maven.
Les dépendances Maven seront téléchargées uniquement la première fois sur la machine hôte.

### Maven & Postgres natif
Installer PostgreSQL puis crée la base de données :

```shell
psql -U postgres -c "CREATE DATABASE bibal;"
```

Exporter les variables d'environnement :

```shell
# Sur les systèmes UNIX :
export DB_DRIVER=postgresql
export DB_HOST=localhost
export DB_PORT=5432
export DB_NAME=bibal
export DB_USER=postgres
export DB_PASSWORD=postgres

# Sur Windows :
set DB_DRIVER=postgresql
set DB_HOST=localhost
set DB_PORT=5432
set DB_NAME=bibal
set DB_USER=postgres
set DB_PASSWORD=postgres
```

Lancer l'application en utilisant le goal Maven `spring-boot:run` :

```shell
mvn spring-boot:run
```

## Versions
- Java: OpenJDK 1.11
- Maven: 3.6
- Docker: 19.03.4-ce
- Docker Compose: 1.24.1

## Auteurs
- Lionel Legrand
- Pierre Duclou